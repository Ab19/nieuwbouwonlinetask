<?php

/* @var $this yii\web\View */

$this->title = 'Estimator ||';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">Estimate the price of a house in Netherlands</p>

        <p><a class="btn btn-lg btn-success" href="/house">Go</a></p>
    </div>

</div>
