<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>The requester</h1>

<table class="table" style="width:100%">
  <tr>
    <th>Email</th>
    <th>First name</th>
    <th>Addition</th> 
    <th>Last name</th>
    <th>Phone number</th>

  </tr>
    <tr>
        <td><?= Html::encode("{$post['email']}") ?></td>
        <td><?= Html::encode("{$post['firstname']}") ?></td>
        <td><?= Html::encode("{$post['addition']}") ?></td>
        <td><?= Html::encode("{$post['lastname']}") ?></td>
        <td><?= Html::encode("{$post['phonenumber']}") ?></td>    
    </tr>

<?php

/* @var $this yii\web\View */

$this->title = 'Estimator | report';
?>
</table>

<table class="table" style="width:100%">
  <tr>
    <th>Postal code</th>
    <th>House number</th>
    <th>House number addition</th> 
    <th>Square</th>

  </tr>
    <tr>
        <td><?= Html::encode("{$post['postalcode']}") ?></td>
        <td><?= Html::encode("{$post['housenumber']}") ?></td>
        <td><?= Html::encode("{$post['housenumber_addition']}") ?></td>
        <td><?= Html::encode("{$post['square']}") ?></td>  
    </tr>


</table>

<h3>Reason:</h3>
<p>
    <?php if ($post['reason']=='sell') { echo 'I want to sell my house';}
        elseif ($post['reason']=='buy') { echo 'I want to buy this house';}
        else{echo 'I’m just curious about the price';}
        ?>
</p>


<h3>In what time do you want to move:</h3>
<p>
    <?php if ($post['move']=='asap') { echo 'As fast as possible';}
        elseif ($post['move']=='three') { echo 'Within 3 months';}
        elseif ($post['move']=='six') { echo 'Within 6 months';}
        else{echo 'Within 1 year';}
        ?>
</p>


<h3>Type of house:</h3>
<p>

    <?php if ($post['type']=='appartment') { echo 'Appartment';}
        elseif ($post['type']=='detachedhouse') { echo 'Detached house';}
        elseif ($post['type']=='semidetachedhouse') { echo 'Semi detached house';}
        elseif ($post['type']=='terracedhouse') { echo 'Terraced house';}
        else{echo 'Corner house ';}
        ?>
</p>

<hr>    
<h1>The estimated price is: <?= Html::encode("{$price}") ?> &euro;</h1>

<hr>
<h1>The closer ten houses</h1>
<table class="table" style="width:100%">
  <tr>
    <th>N°</th>
    <th>Street name</th>
    <th>House number</th> 
    <th>House number addition</th>
    <th>City</th>
    <th>Price</th>
    <th>Floor</th>
    <th>Status</th>
  </tr>
  <?php $cpt=1?>
  <?php foreach ($houses as $house): ?>
    <tr>
        <td><?php echo $cpt; $cpt=$cpt+1;?></td>
        <td><?= Html::encode("{$house->streetname}") ?></td>
        <td><?= Html::encode("{$house->housenumber}") ?></td>
        <td><?= Html::encode("{$house->addition}") ?></td>
        <td><?= Html::encode("{$house->city}") ?></td>
        <td><?= Html::encode("{$house->price}") ?></td>
        <td><?= Html::encode("{$house->floor}") ?></td>
        <td><?= Html::encode("{$house->status}") ?></td>    
    </tr>
<?php endforeach; ?>

</table>
<hr>
