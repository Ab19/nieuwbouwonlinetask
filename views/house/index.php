<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
?>

<?php

/* @var $this yii\web\View */

$this->title = 'Estimator ||';
?>
<?php $form = ActiveForm::begin(); ?>

    <h1>Step 1 -------------------------------------------------------------------------------------</h1>

    <?= $form->field($model, 'postalcode')->hint('required format 9999AA')->label('Postal code *')  ?>

    <?= $form->field($model, 'housenumber')->label('House number *') ?>

    <?= $form->field($model, 'housenumber_addition')->label('House number addition') ?>

    <?= $form->field($model, 'square')->label('Square(meter) *') ?>

    <?= $form->field($model, 'reason')->label('Reason *')->dropDownList(
        ['sell' => 'I want to sell my house',
        'buy' => 'I want to buy this house',
        'curious' => 'I’m just curious about the price ']); 
        ?>

    <?= $form->field($model, 'move')->label('In what time do you want to move')->dropDownList(
        ['asap' => 'As fast as possible',
        'three' => 'Within 3 months',
        'six' => 'Within 6 months',
        'twelve' => 'Within 1 year']); 
        ?>

    <h1>Step 2 -------------------------------------------------------------------------------------</h1>

    <?= $form->field($model, 'type')->label('Choose your type of house')->dropDownList(
        ['appartment' => 'Appartment',
        'detachedhouse' => 'Detached house',
        'semidetachedhouse' => 'Semi detached house',
        'terracedhouse' => 'Terraced house',
        'cornerhouse ' => 'Corner house ']); 
        ?>




    <hr>

    <h1>Step 3 -------------------------------------------------------------------------------------</h1>

    Give your personal information 
    <br><br>

    <?= $form->field($model, 'email')->input('email')->label('Email *') ?>

    <?= $form->field($model, 'firstname')->label('First name *') ?>

    <?= $form->field($model, 'addition')->label('Addition *') ?>

    <?= $form->field($model, 'lastname')->label('Lastname *') ?>

    <?= $form->field($model, 'phonenumber')->label('Phone number *') ?>



    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>