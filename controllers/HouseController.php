<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\House;
use yii\db\Query;
use app\models\EstimateForm;
use yii\helpers\VarDumper;

class HouseController extends Controller
{

    public function actionIndex()
    {
        $model = new EstimateForm();

        $request = Yii::$app->request;

        $post = $request->post();
        // equivalent to: $get = $_GET;
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {


            $post=$request->post()['EstimateForm'];
            
            //VarDumper::dump($post);
            //die();

            $postalcode=$post['postalcode']; 
            $floor=$post['square'];

            $houses=$this->tenHouses($postalcode);     
            $price=round($this->average($houses)*$floor);


            //die();


             return $this->render('report', [
                'houses' => $houses,
                'price'=> $price,
                'post'=>$post,
            ]);
     
        } else {
            // either the page is initially displayed or there is some validation error
            return $this->render('index', ['model' => $model]);
        }
    }


    private function average($houses){
        $sum=0;
        foreach ($houses as $house){
            $ppm=intval($house['price'])/$house['floor'];
            $sum=$sum+$ppm;
        }

        $average=$sum/count($houses);
        return $average;
    }

    private function tenHouses($postalcode){
        

        $tenhouses=[];
 
       
        $left=10;
        $l=4;

        $ten=false;

        while (!$ten and $l!=1){

            $s_cp=substr($postalcode , 0 , $l );
            $sql = "SELECT * FROM `house` WHERE postalcode LIKE '".$s_cp."%' limit ".$left;
            $model = House::findBySql($sql)->all();

            $tenhouses = array_merge($tenhouses, $model);

            if (count($tenhouses)<10){
                $left=10-count($tenhouses);
            }
            else {
                $ten=true;
            }
            $l=$l-1;        
        }

        return $tenhouses;
    }

}