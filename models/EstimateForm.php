<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EstimateForm extends Model
{
    public $postalcode;
    public $housenumber;
    public $housenumber_addition;
    public $square;
    public $reason;
    public $move;

    public $type;

    public $email;
    public $firstname;
    public $addition;
    public $lastname;
    public $phonenumber;


    public function rules()
    {
        return [
        
         	[['postalcode', 'housenumber', 'square', 'email', 'firstname', 'addition', 'lastname', 'phonenumber'], 'required'],

	    	//[['postalcode', 'housenumber'], 'unique', 'targetAttribute' => ['postalcode', 'housenumber','housenumber_addition']],

	        //Still I need a validation for the postalcode in here
            ['postalcode', 'match', 'pattern' => '/^[0-9]{4}[A-Z]{2}$/'],

	        ['postalcode', 'string'],

            [['square'],'integer'],
            ['email','email'],
            ['phonenumber', 'number'],
        
        ];
    }
}